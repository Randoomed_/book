package com.ucbcba.book.repositories;

import com.ucbcba.book.entities.Book;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface BookRepository extends CrudRepository<Book,Integer> { }