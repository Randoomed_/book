package com.ucbcba.book.repositories;

import com.ucbcba.book.entities.BookCategory;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface BookCategoryRepository extends CrudRepository<BookCategory, Integer> { }