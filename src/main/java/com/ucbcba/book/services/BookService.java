package com.ucbcba.book.services;

import com.ucbcba.book.entities.Book;

public interface BookService {

    Iterable<Book>listAllBooks();
    Book findBook(Integer id);
    void saveBook(Book book);
    void deleteBook(Integer id);
}
